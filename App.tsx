import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import styled from 'styled-components/native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();
import ScreenHome from './screens/home';
import Screenbway from './screens/subway';

// const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={ScreenHome} />
        <Stack.Screen name="Subway" component={Screenbway} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const BtnContainer = styled.View`
  flex: 1;
  width: 100%;
  justify-content: center;
  align-items: center;
  background-color: red;
`;

const HomeBtn = styled.View`
  flex: 0.5;
  width: 100%;
  justify-content: center;
  align-items: center;
  background-color: skyblue;
`;

const SubwayBtn = styled.View`
  flex: 0.5;
  width: 100%;
  justify-content: center;
  align-items: center;
  background-color: #f5fcff;
`;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

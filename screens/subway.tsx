import { useNavigation } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, Text, View } from 'react-native';
import styled from 'styled-components/native';
import { RootStackParam } from './home';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import axios from 'axios';

const ScreenSubway = () => {
  const navigation = useNavigation<NativeStackNavigationProp<RootStackParam>>();

  const authKey = process.env.SUBWAY_AUTHKEY;
  const resType = "json";

  const getSubwayInfoes = () => {
    console.log("지하철 정보 가져오기")
    const dataType = "realtimeStationArrival";
    const subwayName = "삼전";

    // 지하철 실시간 도착 정보
    const url1 = `http://swopenAPI.seoul.go.kr/api/subway/${authKey}/${resType}/${dataType}/0/5/${subwayName}`;

    // 지하철역 정보
    const subwayNumberName = "9호선";
    const url2 = `http://openapi.seoul.go.kr:8088/${authKey}/${resType}/SearchSTNBySubwayLineInfo/1/5/ / /${subwayNumberName}`;

    axios.get<any, any>(`${url1}`)
      .then((res) => {
        if (res.status === 200 || res.status === 201) {
          console.log(res["data"])
        }
        console.log(res.status, "===결과2");
      }).catch((err) => {
        console.log(err);
      })
  }

  // 지하철 호선, 해당 지하철 정보 가져오기
  const getSubwayInfoes2 = () => {
    console.log("호선 및 지하철 정보 가져오기")
    const dataType = "SearchSTNBySubwayLineInfo";
    const subwayName = "삼전";

    // 지하철역 정보
    const subwayNumberName = "9호선";
    const url = `http://openapi.seoul.go.kr:8088/${authKey}/${resType}/${dataType}/1/5/ / /${subwayNumberName}`;

    axios.get<any, any>(`${url}`)
      .then((res) => {
        if (res.status === 200 || res.status === 201) {
          console.log(res["data"])
        }
        console.log(res.status, "===결과2");
      }).catch((err) => {
        console.log(err);
      })
  }

  return (
    <>
      <View style={styles.container}>
        <Button
          title="Go to HomeScreen"
          onPress={() => navigation.navigate('Home')}
        />
        <BtnContainer>
          <Button
            title="get subway infoes1"
            onPress={() => getSubwayInfoes()}
          />
        </BtnContainer>
        <BtnContainer color="yellow">
          <Button
            title="get subway infoes2"
            onPress={() => getSubwayInfoes2()}
          />
        </BtnContainer>
        <StatusBar style="auto" />
      </View>
    </>
  );
};

export default ScreenSubway;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const BtnContainer = styled.View<{ color?: string }>`
  flex: 0.5;
  width: 100%;
  justify-content: center;
  align-items: center;
  background-color: ${(props: any) => props.color || '#a2a2a2'};
`;
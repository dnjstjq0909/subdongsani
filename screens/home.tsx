import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, Text, View } from 'react-native';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

export type RootStackParam = {
  Home: undefined;
  Subway: undefined;
};

const ScreenHome = () => {
  const navigation = useNavigation<NativeStackNavigationProp<RootStackParam>>();

  return (
    <>
      <View style={styles.container}>
        <BtnContainer>
          <HomeBtn>
            {/* <Text>Home</Text> */}
            <Button
              title="Go to HomeScreen"
              onPress={() => navigation.navigate('Home')}
            />
          </HomeBtn>
          <SubwayBtn>
            {/* <Text>Subway</Text> */}
            <Button
              title="Go to SubwayScreen"
              onPress={() => navigation.navigate('Subway')}
            />
          </SubwayBtn>
        </BtnContainer>
        <StatusBar style="auto" />
      </View>
    </>
  );
};

export default ScreenHome;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const BtnContainer = styled.View`
  flex: 1;
  width: 100%;
  justify-content: center;
  align-items: center;
  background-color: red;
`;

const HomeBtn = styled.View`
  flex: 0.5;
  width: 100%;
  justify-content: center;
  align-items: center;
  background-color: skyblue;
`;

const SubwayBtn = styled.View`
  flex: 0.5;
  width: 100%;
  justify-content: center;
  align-items: center;
  background-color: #f5fcff;
`;